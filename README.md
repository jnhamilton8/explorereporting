This Play2/Scala website is currently being built.

When it is complete, it will allow the user to create Exploratory Session Reports, edit the notes as they go along, and retrieve them later for viewing.

More functionality may be added as time goes on.

**Development Instructions**

When you first start developing:

- Install sbt (http://www.scala-sbt.org/)
- Install node (https://nodejs.org)
- Run `npm install`
- (You may need to install gulp explicitly with -g for ease of use - `npm install gulp -g`)
- Run `gulp build`

From this point onwards you only need to do the following in the future:

- Run `gulp watch &`
- Run `sbt run`

The webserver will now be running, and changes made to JS or less files in the assets directory will automatically be picked up and passed to play, which will serve them. Play will also take care of any changes made to scala files in the app directory, making for easy development.

**JS TDD**
When you are writing tests for your javascript, you can execute all js tests with `gulp test-javascript`, and this can be done even while the server is running without causing any issue.

