var browserify = require('browserify');
var babelify   = require('babelify');
var envify     = require('envify/custom');
var source     = require('vinyl-source-stream');
var buffer     = require('vinyl-buffer');
var gulp       = require('gulp');
var utils      = require('gulp-util');
var sourcemaps = require('gulp-sourcemaps');
var uglify     = require('gulp-uglify');
var watch      = require('gulp-watch');
var batch      = require('gulp-batch');
var eslint     = require('gulp-eslint');
var addsrc     = require('gulp-add-src');
var concatCss  = require('gulp-concat-css');
var minifyCss  = require('gulp-minify-css');
var less       = require('gulp-less');
var mocha      = require('gulp-mocha');
var resolve    = require('resolve');
var del        = require('del');
var _          = require('lodash');

var debugMode = utils.env.production === undefined;
var NODE_ENV = debugMode ? 'development' : 'production';

var jsTestRoot = "./assets/js-tests/";
var jsRoot = "./assets/javascripts/";
var compiledAssetsTarget = './app/assets/';
var stylesheetsRoot = './assets/stylesheets/';
var jsEntryPoint = 'main.js';

// This function is here to ensure gulp doesn't crash out when watching if a user makes a syntax error.
function swallowStreamingErrors (error) {
    utils.log(error.toString());
    this.emit('end');
}

// Used when packaging js libraries and application code separately.
function getNPMPackageIds() {
    return _.keys(require('./package.json').dependencies) || [];
}

//verify javascript using eslint
gulp.task('lint-javascript', function() {

    return gulp.src(jsRoot + '*')
        .pipe(eslint([".eslintrc"]))
        .pipe(eslint.format())

});

//running javascript tests
gulp.task('test-javascript', ['lint-javascript'], function () {

    return gulp.src(jsTestRoot + '*/**.js', {read: false})
        .pipe(mocha({
            reporter: 'spec',
            compilers: {
                js: require('babel-core/register')
            }
        }));

});

//calls other functions
gulp.task('build-javascript', ['build-lib-javascript','build-app-javascript'], function () { });

//minifies external libraries and places in /app/assets/javascripts
gulp.task('build-lib-javascript', function () {
    var builder = browserify({
        debug: debugMode
    });
    getNPMPackageIds().forEach(function(id) {
        builder.require(resolve.sync(id), { expose: id });
    });

    return builder
        .transform(envify({NODE_ENV: NODE_ENV}), {global: true})
        .bundle()
        .on('error', swallowStreamingErrors)
        .pipe(source('lib.min.js'))
        .pipe(buffer())
        // we only want sourcemaps when debugging, and only want to uglify in prod builds.
        .pipe(debugMode ? sourcemaps.init({loadMaps: true}) : utils.noop())
        .pipe(debugMode ? utils.noop() : uglify())
        .pipe(debugMode ? sourcemaps.write('./') : utils.noop())
        .pipe(gulp.dest(compiledAssetsTarget + 'javascripts/'));
});

//minifies internal application js files and places in /app/assets/javascripts
gulp.task('build-app-javascript', ['lint-javascript'], function () {
    var builder = browserify(jsRoot + jsEntryPoint, {
        debug: debugMode
    });
    getNPMPackageIds().forEach(function(id) {
        builder.external(id);
    });

    return builder
        .transform(babelify.configure({
            sourceRoot: compiledAssetsTarget + "/javascripts"
        }))
        .transform(envify({NODE_ENV: NODE_ENV}), {global: true})
        .bundle()
        .on('error', swallowStreamingErrors)
        .pipe(source('app.min.js'))
        .pipe(buffer())
        // we only want sourcemaps when debugging, and only want to uglify in prod builds.
        .pipe(debugMode ? sourcemaps.init({loadMaps: true}) : utils.noop())
        .pipe(debugMode ? utils.noop() : uglify())
        .pipe(debugMode ? sourcemaps.write('./') : utils.noop())
        .pipe(gulp.dest(compiledAssetsTarget + 'javascripts/'));
});

//copies the bootstrap fonts to /app/assets/stylesheets/fonts/
gulp.task('copy-fonts', function() {

    return gulp.src('./node_modules/bootstrap/fonts/*.*')
        .pipe(gulp.dest('./app/assets/fonts/'))

});

//transpiles less files and css files into one file: main.less and places in /app/assets/stylesheets/
gulp.task('build-less', function(){

    var concatCssOptions = {
        rebaseUrls: false
    };

    var minifyCssOptions = {
        keepSpecialComments: false,
        mediaMerging:        false,
        advanced:            false,
        rebase:              false
    };

    return gulp.src(stylesheetsRoot + '*.less')
        .pipe(less())
        .pipe(addsrc.append(stylesheetsRoot + '*.css'))
        .pipe(concatCss('main.css', concatCssOptions))
        .pipe(buffer())
        .pipe(debugMode ? utils.noop() : minifyCss(minifyCssOptions))
        .on('error', swallowStreamingErrors)
        .pipe(gulp.dest(compiledAssetsTarget + 'stylesheets/'));

});

//the main build task.  When run, will call the respective tasks as listed
gulp.task('build', ['test-javascript', 'build-javascript', 'build-less', 'copy-fonts'], function () {

    var mode = debugMode
        ? utils.colors.red('debug')
        : utils.colors.green('production');

    utils.log("Mode     '" + mode + "'...");

});

//gulp watch task.  When used will detect changes in the js and stylesheets as noted and run the respective build tasks.
gulp.task('watch', function () {

    watch(jsRoot + '**', batch(function(events, done) {
        gulp.start('build-app-javascript', done);
    }));

    watch(stylesheetsRoot + '**', batch(function(events, done) {
        gulp.start('build-less', done);
    }));

});

gulp.task('clean', function() {
    return del([compiledAssetsTarget + "**"]);
});


