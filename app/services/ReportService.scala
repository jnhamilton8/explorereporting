package services

import com.google.inject.Inject
import models.ReportsDAO
import models.Report
import scala.concurrent.Future

class ReportService @Inject()(reportsDAO: ReportsDAO) {

  def saveReport(report: Report): Future[Option[Long]] = {
    reportsDAO.saveReport(report)
  }

  def deleteReport(sessionId: Long): Future[Int] = {
    reportsDAO.deleteReport(sessionId)
  }

  def getReportById(sessionId: Long): Future[Option[Report]] = {
    reportsDAO.getReportById(sessionId)
  }

  def getAllReports() = {
    reportsDAO.getAllReports()
  }

  def deleteAllReports(): Future[Int] = {
    reportsDAO.deleteAllReports()
  }
}