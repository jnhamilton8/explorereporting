package controllers
import javax.inject._

import play.api.mvc._
import play.api.libs.json._
import models.{Report}
import services.ReportService
import scala.concurrent.Future
import play.api.libs.json.Reads._
import play.api.libs.functional.syntax._
import scala.concurrent.ExecutionContext.Implicits.global

@Singleton
class ApplicationController @Inject()(reportService: ReportService)  extends Controller {

  def index = Action {
    Ok(views.html.index())
  }

  //for some reason it fails validation when using a Long type in readNullable!  So we have to
  //do horrid things to map it

  implicit val addReportReads: Reads[Report] = (
    (JsPath \ "sessionId").readNullable[String].map(_.map(_.toLong)) and
      (JsPath \ "sessionName").read[String] and
      (JsPath \ "testerName").read[String] and
      (JsPath \ "date").read[String] and
      (JsPath \ "jiraRef").read[String] and
      (JsPath \ "duration").read[String] and
      (JsPath \ "environment").read[String] and
      (JsPath \ "notes").readNullable[String]
    )(Report.apply _)

  def add_report = Action.async(BodyParsers.parse.json) { implicit request =>

    val reportData = request.body.validate[Report]

    reportData.fold(

      errors => Future.successful(BadRequest("Invalid report data, failed json parsing, please check all required fields have been completed " + "\n" + JsError.toJson(errors))),

      newReport => {
        val sessionId = reportService.saveReport(newReport).recover({
          case e: Exception => InternalServerError("The report did not save: " + e.getStackTrace)
            throw e
        })
        sessionId.map({
          case Some(id) => Ok(Json.obj("sessionId" -> id))
          case None => Ok("Report updated")
        })
      }
    )
  }
}

//reports are now inserted & updated fine, but need to change the logic so on success, populate ID from database
// into session id in the form
//on failure present a message that the save failed in the GUI