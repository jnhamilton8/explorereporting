package models

import com.google.inject.{Inject}
import play.api.db.slick.DatabaseConfigProvider
import scala.concurrent.Future

class ReportsDAO @Inject()(protected val dbConfigProvider: DatabaseConfigProvider) extends DAOSlick {

  import driver.api._

  //will return the new session_id on insert, and None on update
  def saveReport(report: Report): Future[Option[Long]] = {

    val insertQuery = (reportsTable returning reportsTable.map(_.sessionId)).insertOrUpdate(report).transactionally
    dbConfig.db.run(insertQuery)
  }

  def deleteReport(sessionId: Long): Future[Int] = {
    dbConfig.db.run(reportsTable.filter(_.sessionId === sessionId).delete.transactionally)
  }

  def getReportById(sessionId: Long): Future[Option[Report]] = {
    dbConfig.db.run(reportsTable.filter(_.sessionId === sessionId).result.headOption.transactionally)
  }

  def getAllReports(): Future[List[Report]] = {
    dbConfig.db.run(reportsTable.to[List].result.transactionally)
  }

  def deleteAllReports(): Future[Int] = {
    dbConfig.db.run(reportsTable.delete.transactionally)
  }
}