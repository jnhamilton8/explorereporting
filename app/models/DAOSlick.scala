package models

import play.api.db.slick.HasDatabaseConfigProvider
import slick.driver.JdbcProfile

/**
  * This trait can be mixed in with other DAOs to use the report table definition and slick db config
  */

trait DAOSlick extends ReportDBTableDefinitions with HasDatabaseConfigProvider[JdbcProfile] {

}
