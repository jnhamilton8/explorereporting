package models

case class Report(sessionId: Option[Long], sessionName: String, testerName: String, date: String, jiraRef: String,
                  duration: String, environment: String, notes: Option[String])

trait ReportDBTableDefinitions {

  import slick.driver.PostgresDriver.api._

  class Reports(tag: Tag) extends Table[Report](tag, "reports") {

    def sessionId = column[Long]("session_id", O.PrimaryKey, O.AutoInc)
    def sessionName = column[String]("session_name")
    def testerName = column[String]("tester_name")
    def date = column[String]("date")
    def jiraRef = column[String]("jira_ref")
    def duration = column[String]("duration")
    def environment = column[String]("environment")
    def notes = column[Option[String]]("notes")

    def * = (sessionId.?, sessionName, testerName, date, jiraRef, duration, environment, notes) <> (Report.tupled, Report.unapply)
  }

  lazy val reportsTable = TableQuery[Reports]

}