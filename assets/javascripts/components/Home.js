
import React from 'react'
import {Button, ButtonToolbar, PageHeader } from 'react-bootstrap'
import { LinkContainer } from 'react-router-bootstrap'


export default class Home extends React.Component {

    pageHeader() {
        return (
                <PageHeader id="welcome">Welcome to Exploratory Reporter</PageHeader>
        )
    }


    buttons() {
        return (
            <div>
                <ButtonToolbar>
                    <LinkContainer to="/new_session">
                        <Button id='new_session' className="homeButtonStyle" bsStyle="primary" bsSize="lg">Create new session</Button>
                    </LinkContainer>
                </ButtonToolbar>
                <ButtonToolbar>
                    <LinkContainer to="/edit_session">
                        <Button id='edit_session' className="homeButtonStyle" bsStyle="primary" bsSize="lg">Edit existing session</Button>
                    </LinkContainer>
                </ButtonToolbar>
            </div>
        )
    }


    render() {
        return (
        <div className="container container-fixed">

            {this.pageHeader()}
            {this.buttons()}

        </div>
        )
    }
}

