import React from 'react'
import * as ReactBootstrap from 'react-bootstrap';
import { Panel, Button, ControlLabel, FormGroup, FormControl, Col } from 'react-bootstrap'
import { Field, reduxForm } from 'redux-form'
import SingleDatePickerWrapper from './SingleDatePickerWrapper';

var Alert = ReactBootstrap.Alert;

const panel_title = (
    <h3><b>New Session Report</b></h3>
);

const textInput = (props) => {
    var {input} = props

    return (
        <FormGroup>
            <Col componentClass={ControlLabel} sm={props.labelCol}>
                {props.label}
            </Col>
            <Col sm={props.fieldCol}>
            <FormControl
                    name={input.name}
                    onBlur={input.onBlur}
                    onChange={input.onChange}
                    readOnly={props.readOnly}
                    placeholder={props.placeholder}
            >
            </FormControl>
            </Col>
        </FormGroup>
    )
};

const textAreaInput = (props) => {
    var {input} = props

    return (
        <FormGroup>
            <Col componentClass={ControlLabel} sm={props.labelCol}>
                {props.label}
            </Col>
            <Col sm={props.fieldCol}>
                <FormControl
                    name={input.name}
                    onBlur={input.onBlur}
                    onChange={input.onChange}
                    value={input.value}
                    componentClass="textarea" rows={props.rows} style={{resize: "vertical"}} type="text"
                >
                </FormControl>
            </Col>
        </FormGroup>
    )
};


const renderDatePicker = (props) => {
    var {input} = props
    return (
        <FormGroup>
            <Col componentClass={ControlLabel} sm={props.labelCol}>
                {props.label}
            </Col>
            <Col sm={props.fieldCol} onBlur={input.onBlur}
                 onChange={input.onChange}
                 value={input.value}>
                <div className="test">
                <SingleDatePickerWrapper />
                    </div>

            </Col>
        </FormGroup>
    )
};

class AddReport extends React.Component {

    exploratory_report_form() {
        const { handleSubmit, reset} = this.props;
        return (
            <Panel header={panel_title}>

                <form onSubmit={this.props.onSubmit} className="form-horizontal">

                    <Field name="sessionId" label="SessionID" readOnly="true" labelCol={2} fieldCol={4} component={textInput}
                           type="text"/>
                    <Field name="sessionName" label="Session Name:" labelCol={2} fieldCol={4}
                           component={textInput} type="text"/>
                    <Field name="testerName" label="Tester Name:" labelCol={2} fieldCol={4} component={textInput}
                           type="text"/>
                    <Field name="date" label="Date:" labelCol={2} fieldCol={2} component={renderDatePicker}/>
                    <Field name="jiraRef" label="JIRA Ref:" labelCol={2} fieldCol={2} component={textInput}
                           type="text"/>
                    <Field name="duration" placeholder="H:M" label="Duration:" labelCol={2} fieldCol={2} component={textInput}
                           type="text"/>
                    <Field name="environment" label="Environment:" labelCol={2} fieldCol={6} rows={3}
                           component={textAreaInput} type="text"/>
                    <Field name="notes" label="Notes:" labelCol={2} fieldCol={9} rows={20}
                           component={textAreaInput} type="text"/>
                    {this.getReportSaveState()}
                    <Button type="submit" className="btn btn-primary" onClick={handleSubmit}>
                        Save
                    </Button>
                    <Button type="reset" className="btn btn-default" onClick={reset}>
                        Clear
                    </Button>

                </form>
            </Panel>
        )
    }

    getReportSaveState(){
        console.log(this.props.saveResult);
        switch(this.props.saveResult) {
            case "SUCCESS":
                return <Alert className="no-padding-alert" bsStyle="success">Report successfully saved</Alert>;
            case "FAILURE":
                return <Alert className="no-padding-alert" bsStyle="danger">Report failed to save, please try again</Alert>;
            default:
                return <div></div>
        }
    }

    render() {
        return (
            <div className="container container-fixed">
                {this.exploratory_report_form()}
            </div>
        )
    }
}

export default reduxForm({
    form: 'addReport'
})(AddReport)