
import React from 'react'
import ResultsTable from './ResultsTable'
import Searchbar from './Searchbar'


export default class EditSessionReport extends React.Component {

    render() {
        return (
            <div className="container container-fixed">
                <Searchbar />
                <ResultsTable data={SESSIONS}/>
            </div>
        )
    }
}

//hard coded test data just for now until implement database
var SESSIONS = [
    {session_id: '1', session_name: 'CPOF-1234 testing apples', session_tester: 'Jane Hamilton' },
    {session_id: '2', session_name: 'CPOF-5678 testing lunch', session_tester: 'Erik M' },
    {session_id: '3', session_name: 'CPOF-3677 testing chocolate', session_tester: 'Jane Hamilton' },
    {session_id: '4', session_name: 'CPOF-8642 testing biscuits', session_tester: 'Erik M' },
    {session_id: '5', session_name: 'CPOF-1747 testing pie', session_tester: 'Jane Hamilton' },
    {session_id: '6', session_name: 'CPOF-1747 testing cookies', session_tester: 'John Smith' },
    {session_id: '7', session_name: 'CPOF-1685 testing cake', session_tester: 'Erik M' },
    {session_id: '8', session_name: 'CPOF-1234 testing bananas', session_tester: 'John Smith' },
    {session_id: '9', session_name: 'CPOF-1234 testing oranges', session_tester: 'John Smith' },
    {session_id: '10', session_name: 'CPOF-1234 testing deep pan pizza', session_tester: 'Jane Hamilton' }
]