
import React from 'react'
import { Navbar} from 'react-bootstrap'



export default class NavBar extends React.Component {
    render() {
        return (
            <div>
                <Navbar staticTop className="navbar-inverse navbar-fixed-top">
                    <Navbar.Header className="navbar-header">
                        <Navbar.Brand className="navbar-brand" id="navBar_main">
                            <a href="/home">Exploratory Reporter</a>
                        </Navbar.Brand>
                    </Navbar.Header>
                </Navbar>
            </div>
        )
    }
}

