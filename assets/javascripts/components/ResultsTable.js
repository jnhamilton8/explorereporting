
import React from 'react'
import {Table, Button, Glyphicon } from 'react-bootstrap'

const getEditButton = (
    <Button bsStyle="primary" bsSize="xs"><Glyphicon glyph="edit"/></Button>
)

const getDeleteButton = (
    <Button bsStyle="danger" bsSize="xs"><Glyphicon glyph="trash"/></Button>
)

const getExportButton = (
    <Button bsStyle="primary" bsSize="xs"><Glyphicon glyph="save"/></Button>
)


export default class ResultsTable extends React.Component {

    results_table() {
        var rows = [];
        if(this.props.data.length != 0) {
            this.props.data.forEach(function(session) {
                rows.push(<tr key={session.session_id}>
                    <td>{session.session_id}</td>
                    <td>{session.session_name}</td>
                    <td>{session.session_tester}</td>
                    <td>{getEditButton}</td>
                    <td>{getDeleteButton}</td>
                    <td>{getExportButton}</td>
                </tr>)
            });
        }


        return (
            <Table striped bordered condensed hover>
                <thead>
                <tr>
                    <th>Session Id</th>
                    <th>Session Name</th>
                    <th>Tester</th>
                    <th>Edit</th>
                    <th>Delete</th>
                    <th>Export</th>
                </tr>
                </thead>
                <tbody>{rows}
                </tbody>
            </Table>
        )
    }

    render() {
        return (
            <div>
                {this.results_table()}
            </div>
        )
    }
}