import React from 'react'
import {Form, FormControl, FormGroup, Col, InputGroup, Panel, Glyphicon, Button} from 'react-bootstrap'


const panel_title = (
    <h3><b>Edit Session Report</b></h3>
)

export default class Searchbar extends React.Component {

    search_bar() {
        return (
            <Panel header={panel_title}>
                <Form horizontal>
                    <FormGroup controlId="search">
                        <Col sm={4}>
                            <InputGroup>
                                <FormControl type="text" placeholder="Search..."/>
                                <InputGroup.Button>
                                    <Button><Glyphicon glyph="search"/></Button>
                                </InputGroup.Button>
                            </InputGroup>
                        </Col>
                    </FormGroup>
                </Form>
            </Panel>
        )
    }

    render() {
        return (
            <div>
                {this.search_bar()}
            </div>
        )
    }
}

