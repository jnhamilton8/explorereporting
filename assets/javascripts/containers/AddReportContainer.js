import React from 'react'
import AddReport from '../components/AddReport'
import { connect } from 'react-redux'
import {addingReport} from '../actions/actions'


class RawAddReportContainer extends React.Component {

     handleSubmit(values) {
         this.props.dispatch(addingReport(values))
     }

     render() {
         return (
             <AddReport  {...this.props} onSubmit={this.handleSubmit.bind(this)}/>
         )
     }
 }

const AddReportContainer = connect(
    state => {
        return {
            saveResult: state.addReportReducer.saveResult
        }
    })(RawAddReportContainer)

export default AddReportContainer