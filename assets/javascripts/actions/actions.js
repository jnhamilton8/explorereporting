import jquery from 'jquery'
import store from '../store/configureStore';

export const ADD_REPORT_SUCCESS = 'ADD_REPORT_SUCCESS';
export const ADD_REPORT_FAILURE = 'ADD_REPORT_FAILURE';

export function addReportSuccess(values) {
    return {
        type: ADD_REPORT_SUCCESS,
        values
    };
}

export function addReportFailure(saveError) {
    return {
        type: ADD_REPORT_FAILURE,
        saveError
    };
}

export function addingReport(values) {

    return (dispatch) => {

        jquery.ajax(add_report_address, {
            type: 'POST',
            dataType: "html",
            contentType: 'application/json',
            data: JSON.stringify(values),

            success: (response) => {
                console.log("SUCCESSFUL RESPONSE: ", response);
                dispatch(addReportSuccess(values));
                console.log("STORE STATE: ", store.getState());
            },
            error: (response) => {
                console.log("SAVE FAILED: ", response.statusText);
                dispatch(addReportFailure(response.statusText));
                console.log("STORE STATE: ", store.getState());
            }
        })
    }
}