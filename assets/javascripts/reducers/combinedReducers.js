import {reducer as formReducer} from 'redux-form';
import { combineReducers } from 'redux'
import addReportReducer from './addReportReducer'

const combinedReducers = combineReducers({
    addReportReducer,
    form: formReducer // <-- redux form
});

export default combinedReducers