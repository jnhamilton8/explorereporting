import {ADD_REPORT_SUCCESS, ADD_REPORT_FAILURE} from '../actions/actions'

const initialState = {
    values: {},
    saveError: undefined,
    saveResult: ""
};


export default function AddReportReducer(state = initialState, action){
    switch (action.type) {
        case 'ADD_REPORT_SUCCESS':
            return {
                ...state.values,
                values: action.values,
                saveResult: "SUCCESS",
                saveError: undefined
            };
        case 'ADD_REPORT_FAILURE':
            return {
                ...state.error,
                values: action.values,
                saveResult: "FAILURE",
                saveError: action.error
            };
        default:
            return state
    }
}