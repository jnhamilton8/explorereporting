import React from 'react'
import ReactDOM from 'react-dom'
import { Router} from 'react-router'
import { browserHistory } from 'react-router'
import Routes from './routes'
import { Provider } from 'react-redux'
import store from './store/configureStore';


const rootElement = document.getElementById('root');

ReactDOM.render(
    <Provider store={store}>
        <Router history={browserHistory}>{Routes}</Router>
    </Provider>,
    rootElement
);


