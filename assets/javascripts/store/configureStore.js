import { createStore, applyMiddleware, compose } from 'redux'
import thunk from "redux-thunk";
import combinedReducers from '../reducers/combinedReducers.js'


let store = createStore(combinedReducers, compose (
    applyMiddleware(thunk),
    window.devToolsExtension ? window.devToolsExtension() : f => f
))

export default store