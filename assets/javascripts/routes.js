
import React from 'react';
import { Route } from 'react-router';

import App from './App';
import AddReportContainer from './containers/AddReportContainer';
import EditSessionReport from './components/EditSessionReport';
import Home from "./components/Home";

export default (
    <Route path="/" component={App}>
        <Route path="home" component={Home} />
        <Route path="new_session" component={AddReportContainer} />
        <Route path="edit_session" component={EditSessionReport} />
    </Route>
);

