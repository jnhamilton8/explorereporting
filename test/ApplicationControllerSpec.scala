import org.scalatestplus.play._
import play.api.test._
import play.api.test.Helpers._

class ApplicationControllerSpec extends PlaySpec with OneAppPerTest {

  "Routes" should {

    "send 404 on a bad request" in  {
      route(app, FakeRequest(GET, "/fake")).map(status(_)) mustBe Some(NOT_FOUND)
    }

  }

  "ApplicationController" should {

    "render the Exploratory Reporter Home page" in {
      val home = route(app, FakeRequest(GET, "/home")).get

      status(home) mustBe OK
      contentType(home) mustBe Some("text/html")
      contentAsString(home) must include ("Exploratory Reporter")
    }

  }

}
