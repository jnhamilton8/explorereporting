import org.scalatest._
import org.scalatestplus.play._

import play.api.test._
import play.api.test.Helpers.{GET => GET_REQUEST, _}

class IntegrationSpec extends PlaySpec with OneServerPerTest with OneBrowserPerTest with ChromeFactory {

  "The Exploratory Reporter application" should {

    "load the home page correctly" in {

      go to ("http://localhost:" + port)

      pageSource must include ("Welcome to Exploratory Reporter")
    }
  }
}
