package models

import org.scalatestplus.play.{OneAppPerSuite, PlaySpec}
import play.api.{Application, Configuration, Mode}
import play.api.inject.guice.GuiceApplicationBuilder

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Future}


/*Please note at the moment the test data is not reset for each test, so they are set up to all be run at once
 * If they need to be run individually, then the sessionIds will need to be modified to Some(1)
  * This will hopefully be changed when Evolutions is introduced*/

class DAOSpec extends PlaySpec with OneAppPerSuite {

 implicit override lazy val app = new GuiceApplicationBuilder()
    .configure(
      Configuration.from(
        Map(
          "slick.dbs.default.driver" -> "slick.driver.H2Driver$",
          "slick.dbs.default.db.driver" -> "org.h2.Driver",
          "slick.dbs.default.db.connectionPool" -> "disabled",
          "slick.dbs.default.db.keepAliveConnection" -> "true",
          "slick.dbs.default.db.url" -> "jdbc:h2:mem:explore_reporter;MODE=PostgreSQL;DATABASE_TO_UPPER=false;INIT=runscript from 'test/resources/h2schema.sql'",
          "slick.dbs.default.db.user" -> "sa",
          "slick.dbs.default.db.password" -> "")))
    .in(Mode.Test)
    .build()

  "ReportsDAO should" should {

    def reportDB =   Application.instanceCache[ReportsDAO].apply(app)

    "Delete a report" in {

      val testReportToDelete = Report(None, "Data for delete report test", "Erik M", "2016-11-14", "CPOF-4444",
        "1.5 hours", "Pulse 4.5.0", Option("These are notes for the delete report test"))

      await(reportDB.saveReport(testReportToDelete))

      val result2 = await(reportDB.deleteReport(1))
      result2 mustEqual 1
      println("The return code for the deletion statement is: " + result2)
    }


    "Return the correct report when retrieving by id" in {

      val testReportToRetrieve = Report(None, "Data for retrieve report test", "Jane Hamilton", "2016-10-01", "CPOF-9999",
        "15 hours", "UAT server", Option("These are notes for the retrieve report test"))

      await(reportDB.saveReport(testReportToRetrieve))

      val result = await(reportDB.getReportById(2))
      result.get.sessionId mustEqual Some(2)
      result.get.sessionName mustEqual "Data for retrieve report test"
      result.get.testerName mustEqual "Jane Hamilton"
      result.get.date mustEqual  "2016-10-01"
      result.get.jiraRef mustEqual "CPOF-9999"
      result.get.duration mustEqual "15 hours"
      result.get.environment mustEqual "UAT server"
      result.get.notes === "Test data for the retrieve report test"
      println("The returned row retrieved by id is: " + result)

    }

    "Insert a new report or if exists, update" in {

      //set up test data

      val testReportToInsert = Report(None, "Test Insert Report", "John Smith", "2016-09-28", "CPOF-5678",
        "4 hours", "Pulse 4.4.0", Option("These are some notes for the insert report test"))
      val testReportToInsert2 = Report(Some(3), "Test Insert Report UPDATE", "John Smith UPDATE", "2016-09-28", "CPOF-5678",
        "4 hours", "Pulse 4.4.0", Option("These are some notes for the insert report test UPDATED"))

      //insert the first record
      val result3 = await(reportDB.saveReport(testReportToInsert))
      result3 mustEqual Some(3)
      println("Original record inserted was: " + testReportToInsert)
      println("Session ID of newly inserted record is: " + result3)

      //send in record to be updated with same primary key (1)
      val result4 = await(reportDB.saveReport(testReportToInsert2))
      result4 mustEqual None //nothing is returned when slick updates using insertOrUpdate

    }

    "Retrieve all reports in the table" in {

      val showUpdate = await(reportDB.getAllReports())

      showUpdate mustEqual List(Report(Some(2),"Data for retrieve report test","Jane Hamilton","2016-10-01","CPOF-9999","15 hours",
        "UAT server",Some("These are notes for the retrieve report test")),
        Report(Some(3),"Test Insert Report UPDATE","John Smith UPDATE","2016-09-28","CPOF-5678","4 hours","Pulse 4.4.0",
          Some("These are some notes for the insert report test UPDATED")))

      println("All records currently in table including updated record: " + showUpdate)

    }
  }

  def await[T](v: Future[T]): T = Await.result(v, Duration.Inf)

}
